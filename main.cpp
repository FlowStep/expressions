#include <iostream>
#include <cassert>

class Number;
class BinaryOperation;

class Visitor {
public:
	virtual void visit(Number const *num) = 0;
	virtual void visit(BinaryOperation const *bo) = 0;
};

class Expression {
public:
	virtual ~Expression() {}
	virtual double evaluate() const = 0;
	virtual void accept(Visitor *vstr) const = 0;
};

class Number : public Expression {
public:
	Number() : num_(.0) {}
	Number(double num) : num_(num) {}

	double evaluate() const {
		return num_;
	}

	void accept(Visitor *vstr) const {
		vstr->visit(this);
	}
private:
	double num_;
};

class BinaryOperation : public Expression {
public:
	BinaryOperation(Expression *left, char op, Expression *right)
		: left_(left), right_(right) {
		assert(op == '+' || op == '-' || op == '*' || op == '/');
		op_ = op;
	}

	double evaluate() const {
		switch (op_) {
		case '+':
			return left_->evaluate() + right_->evaluate();
		case '-':
			return left_->evaluate() - right_->evaluate();
		case '*':
			return left_->evaluate() * right_->evaluate();
		case '/':
			return left_->evaluate() / right_->evaluate();
		default:
			break;
		}
	}

	Expression const *get_left() const {
		return left_;
	}

	Expression const *get_right() const {
		return right_;
	}

	char get_op() const {
		return op_;
	}

	void accept(Visitor *vstr) const {
		vstr->visit(this);
	}
	
private:
	Expression *left_;
	char op_;
	Expression *right_;
};

class Printer : public Visitor {
public:
	void visit(Number const *num) {
		std::cout << num->evaluate();  
	}

	void visit(BinaryOperation const *bo) {
		std::cout << '(';
		bo->get_left()->accept(this);
		std::cout << ' ' << bo->get_op() << ' ';
		bo->get_right()->accept(this);
		std::cout << ')';
	}
};

int main() {
	Expression *a, *b, *c, *d, *e;
	a = new Number(2.0);
	b = new Number(3.0);
	c = new BinaryOperation(a, '+', b);
	d = new BinaryOperation(c, '-', a);
	e = new BinaryOperation(d, '+', c);
	Printer pr;
	e->accept(&pr);
	std::cout << " = " << e->evaluate() << std::endl;
	system("pause");
}